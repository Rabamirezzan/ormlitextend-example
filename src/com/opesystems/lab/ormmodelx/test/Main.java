package com.opesystems.lab.ormmodelx.test;

import java.util.List;

import com.opesystems.dao.AnimalDao;
import com.opesystems.dao.OrmHelper;
import com.opesystems.dao.PersonDao;
import com.opesystems.dao.PersonWhere;
import com.opesystems.tools.ormmodel.db.DBHelper;
import com.opesystems.tools.ormmodel.db.DBInstance;

public class Main {

	public static void main(String[] args) {
		DBHelper helper = new OrmHelper(new MyDatabaseInfo());

		DBInstance instance = helper.getInstance();

		instance.dropAll();
		instance.createAll();
		PersonDao dao = Person.getDao(instance);
		Person p = new Person();
		p.setName("raul");
		dao.save(p);

		p = new Person();
		p.setName("ramirez");
		dao.save(p);

		p = new Person();
		p.setName("bazan");
		dao.save(p);

		List<Person> persons = dao.queryWhere().name().eq("raul").and().age().isNull().query();
		Long age = 0L;
		for (Person person : persons) {
			person.setAge(age += 10);
			dao.update(person);
		}
		System.out.println(String.format("%s", persons));

		AnimalDao aDao = Animal.getDao(instance);
		Animal animal = new Animal();
		animal.setName("Dolly");
		aDao.save(animal);

		List<Animal> animals = aDao.query();
		System.out.println(String.format("%s", animals));
		PersonWhere w = Person.getDao(instance).queryWhere().orderBy().age()
				.desc();
		String statement = w.getPrepareStatementString();
		System.out.println(String.format("%s", statement));
		persons = w.query();
		System.out.println(String.format("%s", persons));
	}
}
