package com.opesystems.lab.ormmodelx.test

import com.j256.ormlite.field.DatabaseField
import com.opesystems.tools.ormmodel.db.xtend.XDaoFun
import org.eclipse.xtend.lib.annotations.Accessors

@XDaoFun(daoPackage="com.opesystems.dao", internalCall=true)
@Accessors
class Person {
	@DatabaseField(generatedId=true)
	private Integer id

	@DatabaseField
	private String name

	@DatabaseField(columnName="edad")
	private Long age

	override toString() {
		name +" "+age
	}
}

@XDaoFun(daoPackage="com.opesystems.dao", internalCall=true)
@Accessors
class Animal {

	@DatabaseField(generatedId=true)
	private Integer id

	@DatabaseField
	private String name

	@DatabaseField
	private String color;

	@DatabaseField(columnName="sonido")
	private String sound;

	override toString() {
		name
	}

}
