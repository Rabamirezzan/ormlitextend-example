package com.opesystems.lab.ormmodelx.test;

import com.opesystems.tools.ormmodel.db.helper.DatabaseInfo;

public class MyDatabaseInfo implements DatabaseInfo {

	@Override
	public String getUrl() {

		return "jdbc:sqlite:C:/temp/xdaotest.db";
	}

}
